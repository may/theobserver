```rust
use theobserver::DeviceQuery;

fn main() {
    let device = DeviceQuery::new()
        .with_pressure()
        .first()
        .unwrap();

    let mut pen = theobserver::watch_device(device);

    loop {
        pen.process_events();
        println!("pressure: {}", pen.pressure());
    }
}
```
